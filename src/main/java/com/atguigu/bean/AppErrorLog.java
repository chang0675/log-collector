package com.atguigu.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppErrorLog {

    private String errorBrief;    //错误摘要
    private String errorDetail;   //错误详情
}