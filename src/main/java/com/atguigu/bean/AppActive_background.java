package com.atguigu.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 用户后台活跃
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppActive_background {
    private String active_source;//1=upgrade,2=download(下载),3=plugin_upgrade

}